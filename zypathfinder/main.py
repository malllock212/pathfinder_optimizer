import os
from drawer import Drawer
from algorithm import dijsktra_shortest_path
from utils import to_node_representation


class PathFinder:
    def __init__(self, _map_filepath: str):
        self._map = self._load_map_from_path(_map_filepath)
        self._drawer = Drawer(self.map)
        self._node_representation = None

    @property
    def node_representation(self):
        if self._node_representation is not None:
            return self._node_representation

        self._node_representation = to_node_representation(self._map)
        return self.node_representation

    def algo_shortest_path(self):
        if self._algo_shortest_path is not None:
            return self._algo_shortest_path

        return dijsktra_shortest_path(self._map)

    @property
    def map(self) -> list:
        assert self._map is not None
        return self._map

    def _load_map_from_path(self, path: str) -> list:
        assert os.path.exists(path)
        map_ = open(path, "r").read()
        map_ = map_.split()
        return map_


def main():
    pf = PathFinder("map_input.in")
    pf._drawer.draw_map()
    nr = pf.node_representation  # noqa: F841, E261
    1 == 1
    print(pf.map)


if __name__ == "__main__":
    main()
