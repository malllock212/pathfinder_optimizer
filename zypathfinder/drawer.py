from PIL import Image, ImageDraw


class Drawer:
    BASE_SIZE = 10
    COLOR_MAP_PICKER = {
        "o": (38, 209, 29),
        "x": (240, 92, 12),
    }
    OUTLINE_SIZE = 1

    def __init__(self, _map: list):
        self.map = _map
        self._image = Image.new("RGB", self.calc_image_size(self.map))

    def calc_image_size(self, map_: list):
        x = len(map_)
        y = len(map_[0])
        assert all(
            [len(_) == y for _ in map_]
        ), "Map must have the same dimensions on all rows. "

        width = x * self.BASE_SIZE
        height = y * self.BASE_SIZE
        return (height + self.OUTLINE_SIZE, width + self.OUTLINE_SIZE)

    @property
    def image(self):
        # TODO: error checking
        assert self._image is not None, "`Image` class cannot be set to None. "
        return self._image

    def draw_map(self):
        x_index, y_index = 0, 0
        draw = ImageDraw.Draw(self.image)

        for map_col in self.map:
            for map_row in map_col:
                color = self.COLOR_MAP_PICKER.get(map_row, (0, 0, 0))
                draw.rectangle(
                    (
                        x_index,
                        y_index,
                        x_index + self.BASE_SIZE,
                        y_index + self.BASE_SIZE,
                    ),
                    fill=color,
                    outline=(255, 255, 255),
                )
                x_index += self.BASE_SIZE

            x_index = 0
            y_index += self.BASE_SIZE

        self.image.save("output_map.jpg", quality=95)
