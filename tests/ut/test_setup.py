import pytest


def test_import_package():
    with pytest.raises(ImportError):
        import zyp  # noqa: F401,E261

    from zypathfinder.drawer import Drawer  # noqa: F401,E261
