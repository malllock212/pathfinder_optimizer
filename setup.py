from setuptools import setup, find_packages


def run_setup():
    setup(
        name="zypathfinder",
        version="0.0.1",
        description="Tool for optimize pathfinding with various set of algorithms",
        author="Zyhu",
        author_email="example.com",
        url="https://not_availabe.com",
        packages=find_packages(
            exclude=["tests", "docs", ".gitignore", "README.MD", "*.txt", "*.pyc"]
        ),
        install_requires=[
            # TODO: make it shorter - some common setuptools config
            "Pillow",
            "pytest",
            "black~=22.0",
            "flake8",
        ],
    )


if __name__ == "__main__":
    run_setup()
