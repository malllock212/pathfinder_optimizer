from enum import IntEnum


# TO_RETHINK: Enum is enough?
class State(IntEnum):
    OUT_OF_BOUND: int = 0
    VISITED: int = 1


# SHOULD BE SOMEWHERE IN FUNCTION
visited = {}


def to_node_representation(map: list):
    class Node:
        def __init__(self, current_node_name=None, start_node=(0, 0)):
            self._start_node = start_node
            # TODO: move to validation step
            # for node name - [x, y] use cords e.g. Node[x, y]
            self._current_node_name = current_node_name or str(
                "Node: ({}, {})".format(start_node[0], start_node[1])
            )
            self._current_node_value = self.current_node_value
            visited[self._current_node_name] = self
            # TODO: decide if delete
            self.UP_NODE = None
            self.DOWN_NODE = None
            self.LEFT_NODE = None
            self.RIGHT_NODE = None

            self._real_value = None
            self.discover_nodes()

        def __repr__(self):
            return f"{id(self)}: {self._current_node_name}"

        @property
        def current_node_value(self):
            assert self._current_node_name is not None, "Node must have name set. "
            nonlocal map
            return map[self._start_node[0]][self._start_node[1]]

        @property
        def complete(self):
            return all(
                [
                    True if getattr(self, x) is not None else False
                    for x in ["UP_NODE", "DOWN_NODE", "LEFT_NODE", "RIGHT_NODE"]
                ]
            )

        @property
        def current_node(self):
            assert not isinstance(
                self._current_node_name, None
            ), "Current node cannot be set to None. "
            return self._current_node_name

        def discover_nodes(self):
            nonlocal map
            global visited
            nodes_mapping = {
                "up_node": lambda x, y: (x - 1, y),
                "down_node": lambda x, y: (x + 1, y),
                "right_node": lambda x, y: (x, y + 1),
                "left_node": lambda x, y: (x, y - 1),
            }

            if self.complete:  # not usable
                return

            for key in nodes_mapping.keys():
                value = getattr(self, key.upper())
                if value is None:
                    node_position = nodes_mapping.get(key)(*self._start_node)

                    visit_name = str(
                        "Node: ({}, {})".format(node_position[0], node_position[1])
                    )
                    if visited.get(visit_name) is not None:
                        setattr(self, key.upper(), visited.get(visit_name))
                        continue

                    if node_position[0] < 0 or node_position[1] < 0:
                        setattr(self, key.upper(), State.OUT_OF_BOUND)
                        continue
                    try:
                        # TODO: check if there is there any better way to make it?
                        #       - without ugly try-catch
                        map[node_position[0]][node_position[1]]
                        new_node = Node(start_node=node_position)
                        setattr(self, key.upper(), new_node)
                    except IndexError:
                        setattr(self, key.upper(), State.OUT_OF_BOUND)

    root = Node()
    if not root.complete:
        raise Exception(f"Couldn't parse given map. {str(root)}")

    return root
